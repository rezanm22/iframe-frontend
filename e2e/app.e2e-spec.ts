import { IframeFrontendPage } from './app.po';

describe('iframe-frontend App', () => {
  let page: IframeFrontendPage;

  beforeEach(() => {
    page = new IframeFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
