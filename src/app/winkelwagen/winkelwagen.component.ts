import { Component, OnInit } from '@angular/core';
import { ProductModel } from 'app/models/ProductModel';
import { WinkelwagenService } from 'app/services/CartService';
import { ShoppingItemModel } from 'app/models/Shopping-item';
import { DialogsService } from 'app/services/DialogService';

@Component({
  selector: 'app-winkelwagen',
  templateUrl: './winkelwagen.component.html',
  styleUrls: ['./winkelwagen.component.css']
})
export class WinkelwagenComponent{
  productsAddedToCart: ShoppingItemModel[]=[];
  totalPrice: number = 0;
  storage = false ? localStorage : sessionStorage;
  constructor(private cartService: WinkelwagenService, private diaService: DialogsService) { 
    
    if(JSON.parse(sessionStorage.getItem("winkelwagen")))
    {
      this.productsAddedToCart = JSON.parse(sessionStorage.getItem("winkelwagen"));
      for(let item of this.productsAddedToCart){
        this.totalPrice += item.product.productPrice * item.quantity;
      }
    }
  }
  updateQuanity(productId: number, currentQuanity: number){
    for(let item of this.productsAddedToCart){
      if(item.product.productId == productId){
          this.totalPrice -= item.product.productPrice * item.quantity;
          item.quantity = currentQuanity;
          this.totalPrice += item.product.productPrice * item.quantity;
        this.storage.setItem('winkelwagen', JSON.stringify(this.productsAddedToCart));
      }
    }
  }
  remove(productNr: number){
    this.diaService.confirm('Bevestigen', 'Weet u zeker dat u dit product wil verwijderen? ').subscribe(res =>{
      if(res != true){}
      else{
        for(let item of this.productsAddedToCart){
          if(item.product.productId == productNr){
            this.productsAddedToCart.splice(
              this.productsAddedToCart.indexOf(item), 1);
              this.totalPrice -= item.product.productPrice * item.quantity;
            this.storage.setItem('winkelwagen', JSON.stringify(this.productsAddedToCart));
          }
        }
      }
    })
  }

  clearWinkelwagen(){
    this.diaService.confirm('Bevestigen', 'Weet u zeker dat de winkelwagen wilt leegmaken?').subscribe(res =>{
      if(res != true){}
      else{
        this.productsAddedToCart.length = 0;
        this.totalPrice = 0;
        this.storage.setItem('winkelwagen', JSON.stringify(this.productsAddedToCart));
      }
    })

  }

}
