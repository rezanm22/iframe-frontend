import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http/';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatProgressSpinnerModule ,MatProgressBarModule,MatRadioModule, MatTableModule, MatSelectModule, MatOptionModule, MatNativeDateModule, MatDatepickerModule, MatCardModule, MatToolbarModule, MAT_DATE_LOCALE, MatPaginator, MatPaginatorModule, MatCheckbox, MatCheckboxModule, MatRadioButton, MatListModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductenComponent } from './producten/producten.component';
import { HomeComponent } from './home/home.component';
import { ProductenBeherenComponent } from './admin/producten-beheren/producten-beheren.component';
import { PortalComponent } from './admin/portal/portal.component';
import { ProductService } from 'app/services/ProductService';
import { WinkelwagenComponent } from './winkelwagen/winkelwagen.component';
import { CreateProductComponent } from 'app/admin/producten-beheren/create-product/create-product.component';
import { UserService } from 'app/services/UserService';
import { AuthService } from 'app/services/AuthService';
import { NoopAnimationPlayer } from '@angular/animations/src/players/animation_player';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { OrdersComponent } from './user/orders/orders.component';
import { UserInfoComponent } from './user/user-info/user-info.component';
import { LoginComponent } from 'app/user/login/login.component';
import { ContactComponent } from './contact/contact.component';
import { CategorieService } from 'app/services/CategorieService';
import { WinkelwagenService } from 'app/services/CartService';
import {MatExpansionModule} from '@angular/material/expansion';
import { UsersManagementComponent } from './admin/users-management/users-management.component';
import { AdminLoginComponent } from 'app/admin/login/login.component';
import {MatTabsModule} from '@angular/material/tabs';
import { CreateUserComponent } from 'app/admin/users-management/create-user/create-user.component';
import { DialogContentComponent } from './home/dialog-content/dialog-content.component';
import { DialogsService } from 'app/services/DialogService';
import { CategoriesManagementComponent } from './admin/categories-management/categories-management.component';
import { CreateCategorieComponent } from './admin/categories-management/create-categorie/create-categorie.component';
import { TestComponent } from './test/test.component';
import { ModifyProductComponent } from 'app/admin/producten-beheren/modify-product/modify-product.component';
import { ModifyUserComponent } from 'app/admin/users-management/modify-user/modify-user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    CheckoutComponent,
    ProductenComponent,
    LoginComponent,
    HomeComponent,
    ProductenBeherenComponent,
    PortalComponent,
    WinkelwagenComponent,
    CreateProductComponent,
    OrdersComponent,
    UserInfoComponent,
    ContactComponent,
    UsersManagementComponent,
    AdminLoginComponent,
    CreateUserComponent,
    DialogContentComponent,
    CategoriesManagementComponent,
    CreateCategorieComponent,
    ModifyProductComponent,
    ModifyUserComponent,
    TestComponent,
  ],
  entryComponents: [DialogContentComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatIconModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    FormsModule,
    MatToolbarModule,
    MatCardModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatPaginatorModule,
    MatCheckboxModule,
    HttpClientModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatListModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      { path: '', redirectTo: 'products', pathMatch: 'full'},
      { path: 'user/login', component: LoginComponent},
      { path: 'user/orders', component: OrdersComponent},
      { path: 'user/info', component: UserInfoComponent},
      { path: 'contact', component: ContactComponent},
      { path: 'admin/login', component: AdminLoginComponent},
      { path: 'admin/portal', component: PortalComponent},
      { path: 'admin/products', component: ProductenBeherenComponent},
      { path: 'admin/products/create', component: CreateProductComponent},
      { path: 'admin/users/create', component: CreateUserComponent},
      { path: 'admin/categories/create', component: CreateCategorieComponent},
      { path: 'admin/categories', component: CategoriesManagementComponent},
      { path: 'admin/users', component: UsersManagementComponent},
      { path: 'winkelwagen', component: WinkelwagenComponent},
      { path: 'products', component: ProductenComponent},
      { path: 'check-out', component: CheckoutComponent},
      { path: 'test', component: TestComponent},
    ]),
    
  ],
  providers: [ProductService, UserService ,AuthService, CategorieService, WinkelwagenService,DialogsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
