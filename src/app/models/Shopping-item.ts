import { ProductModel } from "app/models/ProductModel";

export class ShoppingItemModel{
    constructor(
        public product?: ProductModel,
        public quantity?: number,
    ){}

}