export class UserModel{
    constructor(
        public userId?: number,
        public userName?: string,
        public userEmail?: string,
        public userPassword?: string,
        public role?: string,
        public roleId?:number
    ){}

}