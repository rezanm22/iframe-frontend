export class ProductModel{
    constructor(
        public productId?: number,
        public productName?: string,
        public productPrice?: number,
        public productImgUrl?: string,
        public catId?: number,
    ){}

}