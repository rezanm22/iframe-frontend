export class ShippingModel{
    constructor(
        public name?: string,
        public address?: string,
        public postcode?: string,
        public city?: string,
    ){}

}