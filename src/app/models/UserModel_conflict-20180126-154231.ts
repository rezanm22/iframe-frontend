export class UserModel{
    constructor(
        public userId?: number,
        public userName?: string,
        public userEmail?: string,
        public userPassword?: string,
        public roleName?: string,
        public roleId?:number
    ){}

}