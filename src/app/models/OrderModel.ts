import { ProductModel } from "app/models/ProductModel";
import { ShoppingItemModel } from "app/models/Shopping-item";
import { ShippingModel } from "app/models/ShippingModel";
import { OrderRow } from "app/models/OrderRowModel";

export class OrderModel{
    constructor(
        public orderRowList: OrderRow[],
        public orderTotalPrice: number,
        public shipping?:ShippingModel,
        public userId?:number,
        public orderId?:number,
        public orderDate?:string
    ){}

}