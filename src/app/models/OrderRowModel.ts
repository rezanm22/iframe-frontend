export class OrderRow{
    constructor(
        public productId?: number,
        public quantity?: number,
        public productName?: string
    ){}
}