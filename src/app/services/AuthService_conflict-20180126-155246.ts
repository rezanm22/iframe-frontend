import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { HttpHeaders } from "@angular/common/http/";
import {Headers} from '@angular/http/';
import { UserModel } from "app/models/UserModel";

@Injectable()
export class AuthService{
    password: string = null;
    emailAddress: string = null;
    loggedUserObject: UserModel = null;
    localOrSession: boolean= null;
    constructor() {
        this.getAuthorization();
    }
    apiUrl(){
        return "http://localhost:3000/api/";
    }

    public isAdmin():boolean{
        return (this.isAuthorized() && this.loggedUserObject.roleName === 'admin');
    }
    public isUser():boolean{
        return (this.isAuthorized() && this.loggedUserObject.roleName === 'user');
    }
    public createAuthHeader(): Headers {
        const loginEncoded =  btoa(this.emailAddress + ':' + this.password);
        const headers = new Headers();
        if(this.isAuthorized())
        {
            headers.append('Authorization', 'Basic ' + loginEncoded);
        }
        return headers;
    }

    public createAuthHttpHeader() {
        const loginEncoded =  btoa(this.emailAddress + ':' + this.password);
        let headers = new HttpHeaders();
        if (this.isAuthorized())
        {
            headers = headers.set('Authorization', 'Basic ' + loginEncoded);
        }
        return headers;
    }

    public setAuth(email: string, password: string) {
        this.emailAddress = email;
        this.password = password;
    }
    public isAuthorized() {
        return(this.emailAddress != null && this.password != null);
    }
    public storeAuthorization(local: boolean, user: UserModel) {
        const authorization = {
          email: user.userEmail,
          password: user.userPassword,
          loggedUserObject: user,
          localOrSession: local
        };
        this.emailAddress = user.userEmail;
        this.password = user.userPassword;
        this.loggedUserObject = user;
        this.localOrSession = local;
        const auth = JSON.stringify(authorization);
        const storage = local ? localStorage : sessionStorage;
        storage.setItem('authorization', auth);
    }
    public getAuthorization(): void {
        let authorizationString = sessionStorage.getItem('authorization');
        if (authorizationString == null) {
            authorizationString = localStorage.getItem('authorization');
        }
        if (authorizationString !== null) {
            const authorization = JSON.parse(authorizationString);
            this.emailAddress = authorization['email'];
            this.password = authorization['password'];
            this.loggedUserObject = authorization['loggedUserObject'];
            this.localOrSession = authorization['localOrSession'];
        }
    }
    public getEmployeeModel() {
        return this.loggedUserObject;
    }
    public setNullAfterLogout()
    {
        this.emailAddress = null;
        this.password = null;
        this.loggedUserObject = null;
    }


}