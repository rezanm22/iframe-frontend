import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http/";
import { Observable } from "rxjs/Observable";
import { ProductModel } from "app/models/ProductModel";
import { Http } from "@angular/http";
import { Response } from "@angular/http";
import { AuthService } from "app/services/AuthService";
import { Router } from "@angular/router/";
import {Headers} from '@angular/http/';
import { HttpHeaders } from "@angular/common/http/";
import { HttpParams } from "@angular/common/http/";
import { error } from "selenium-webdriver";
import { MatSnackBar } from "@angular/material";

@Injectable()
export class ProductService{
    productToModify: ProductModel = new ProductModel();
    url: string;
    constructor(private http: Http, private httpN: HttpClient, private auth: AuthService
    ,private router: Router, private snackBar: MatSnackBar)
    { this.url = auth.apiUrl();}


    getAllProducts(){
        return this.httpN.get<ProductModel[]>(this.url+`products/allProducts`);
    }

    getAllProductsHttpClient(): Observable<ProductModel[]> {
    let headers = this.auth.createAuthHttpHeader();
      return this.httpN.get<ProductModel[]>(this.url+`products/allProducts`,{headers:headers});
    }

    public insertNewProduct(model: ProductModel): void
    {
        let headers = this.auth.createAuthHttpHeader();
        let data =
        {
            productName: model.productName,
            productPrice: model.productPrice,
            productImgUrl: model.productImgUrl,
            catId: model.catId
        };
        this.httpN.post(this.url+`products/create`, data,{headers:headers}).subscribe
        (
            data=>{
                if(data != true){
                    this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
                }else{
                    this.snackBar.open('Succesvol','',{duration:1000});
                }
            },
            error =>{
                this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
            }
        );
    }

    public setProductToModify(p: ProductModel)
    {
      this.productToModify = p;
    }
  
    public updateProduct(p: ProductModel)
    {
        
      let data =
      {
          productId: p.productId,
          productName: p.productName,
          productPrice: p.productPrice,
          productImgUrl: p.productImgUrl,
          catId: p.catId
  
      };
      let headers = this.auth.createAuthHttpHeader();
      this.httpN.put(this.url+`products/update/`, data, {headers:headers}).subscribe
      (
        data=>{
            if(data != true){
                this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
            }else{
                this.snackBar.open('Succesvol','',{duration:1000});
            }
           
        },
        error =>{
            this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
        }
      );
    }

    public removeProduct(productId: number){
        let headers = this.auth.createAuthHttpHeader();
        this.httpN.delete(this.url+`products/delete/${productId}`,{headers:headers}).subscribe(
            data=>{
                if(data != true){
                    this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
                }else{
                    this.snackBar.open('Succesvol','',{duration:1000});
                }
            },
            error =>{
                this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
            }
        );
    }
}