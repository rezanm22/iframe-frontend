
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MatDialogRef, MatDialog } from '@angular/material';
import { DialogContentComponent } from 'app/home/dialog-content/dialog-content.component';
import { CreateCategorieComponent } from 'app/admin/categories-management/create-categorie/create-categorie.component';
import { CreateUserComponent } from 'app/admin/users-management/create-user/create-user.component';
import { CreateProductComponent } from 'app/admin/producten-beheren/create-product/create-product.component';

@Injectable()
export class DialogsService {

    constructor(private dialog: MatDialog) { }

    public confirm(title: string, message: string, warning?:string): Observable<boolean> {

        let dialogRef: MatDialogRef<DialogContentComponent>;

        dialogRef = this.dialog.open(DialogContentComponent);

        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;
        if(warning != null){
            dialogRef.componentInstance.warning = warning;
        }
        return dialogRef.afterClosed();
    }
    public createProduct():Observable<boolean>{
        let dialogRef: MatDialogRef<CreateProductComponent>;
        dialogRef = this.dialog.open(CreateProductComponent);

        return dialogRef.afterClosed();
    }

    public createCategorie():Observable<boolean>{
        let dialogRef: MatDialogRef<CreateCategorieComponent>;
        dialogRef = this.dialog.open(CreateCategorieComponent);

        return dialogRef.afterClosed();
    }
    public createUser(): Observable<boolean>{
        let dialogRef: MatDialogRef<CreateUserComponent>;
        dialogRef = this.dialog.open(CreateUserComponent);

        return dialogRef.afterClosed();
    }
}