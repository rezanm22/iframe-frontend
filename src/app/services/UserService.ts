import { Injectable } from "@angular/core";
import { AuthService } from "app/services/AuthService";
import { UserModel } from "app/models/UserModel";
import { Http, Headers,Response } from "@angular/http";
import { HttpClient } from "@angular/common/http/";
import { OrderModel } from "app/models/OrderModel";
import { HttpHeaders } from "@angular/common/http/";
import { Roles } from "app/models/Roles";
import { Alert } from "selenium-webdriver";
import { MatSnackBar } from "@angular/material";

@Injectable()
export class UserService{
    user: UserModel = new UserModel();
    url: string;
    constructor(private auth: AuthService, private httpN: HttpClient, private http: Http, private snackBar: MatSnackBar){
        this.url = this.auth.apiUrl();
    }

    

    getRoles(){
        const headers = this.auth.createAuthHttpHeader();
        return this.httpN.get<Roles[]>(this.url+`users/getRoles`, {headers: headers});
    }
    createUser(user: UserModel){
        const headers = this.auth.createAuthHttpHeader();
        this.httpN.post(this.url+`users/createUser`, user,{headers:headers}).subscribe(data =>{
            if(data != true){
                this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
            }else{
                this.snackBar.open('Succesvol','',{duration:1000});
            }
        },
    error=>{
        this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
    });
    }

    removeUser(userId: number){
        let headers = this.auth.createAuthHttpHeader();
        this.httpN.delete(this.url+`users/delete?userId=${userId}`,{headers:headers}).subscribe(
            data=>{
                if(data != true){
                    this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
                }else{
                    this.snackBar.open('Succesvol','',{duration:1000});
                }
            },
            error =>{
                this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
            }
        );
    }
    getAllUsers(){
        const headers = this.auth.createAuthHttpHeader();
       return this.httpN.get<UserModel[]>(this.url+`users/getAll`, {headers:headers});
    }

    getUserOrders(user: UserModel){
        let data = {
            userId: user.userId,
            userName: user.userName
        }
        const headers = this.auth.createAuthHttpHeader();
        return this.httpN.post<OrderModel[]>(this.url+`orders/ordersByUser`,data, {headers:headers});
    }
    updateUserInfo(user: UserModel){
        const headers = this.auth.createAuthHttpHeader();
        this.httpN.put(this.url+`users/update`, user, {headers: headers}).subscribe(
            data=>{
                if(data != true){
                    this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
                }else{
                    this.snackBar.open('Succesvol','',{duration:1000});
                }
            },
            error =>{
                this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
            }
    );
    }
    login(local: boolean, email: string, password: string){
        const loginEncoded =  btoa(email + ':' + password);
        let headers = new Headers({ 
            'Access-Control-Allow-Origin':'*'
          });
          headers.append('Authorization', 'Basic ' + loginEncoded);
        this.http.get(this.url+`users/login`, {headers: headers}).subscribe(
            (data: Response) =>{
                let res = data.json();
                this.user.userId = res.userId;
                this.user.userName = res.userName;
                this.user.userEmail = res.userEmail;
                this.user.userPassword = res.userPassword;
                this.user.role = res.role;
                this.user.roleId = res.roleId;
                this.auth.storeAuthorization(false, this.user);
            }
        );
    }
    logOut(){
        let storage = false ? localStorage : sessionStorage;
        storage.removeItem('authorization');
        setTimeout(location.reload.bind(location), 100);
    }
    
}