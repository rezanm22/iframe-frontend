import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import {Headers} from '@angular/http/';
import { CategorieModel } from "app/models/CategorieModel";
import { HttpClient } from "@angular/common/http/";
import { AuthService } from "app/services/AuthService";
import { MatSnackBar } from "@angular/material";

@Injectable()
export class CategorieService{
    url: string;
    constructor(private http: HttpClient, private auth: AuthService, private matSnackbar: MatSnackBar){
        this.getAll();
        this.url = auth.apiUrl();
    }

    getAll(){
        return this.http.get<CategorieModel[]>(this.url+`categories/getAll`)
    }

    createCategorie(model: CategorieModel){
        const headers = this.auth.createAuthHttpHeader();
        this.http.post(this.url+`categories/create`,model,{headers:headers}).subscribe(res=>{
            if(res != true){
                this.matSnackbar.open('Er is iets fout gegaan met de server','',{duration:1000});
            }else{
                this.matSnackbar.open('Succesvol','',{duration:1000});
            }
        },error=>{
            this.matSnackbar.open('Er is iets fout gegaan','', {
                duration: 1000
              })
        })
    }
    removeCategorie(catId: number){
        const headers = this.auth.createAuthHttpHeader();
        this.http.delete(this.url+`categories/delete?catId=${catId}`, {headers:headers}).subscribe(res=>{
            if(res != true){
                this.matSnackbar.open('Er is iets fout gegaan met de server','',{duration:1000});
            }else{
                this.matSnackbar.open('Succesvol','',{duration:1000});
            }
        },error=>{
            this.matSnackbar.open('Er is iets fout gegaan','', {
                duration: 1000
              })
        })
    }
}

