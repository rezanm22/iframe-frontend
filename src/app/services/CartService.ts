import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http/";
import { OrderModel } from "app/models/OrderModel";
import { AuthService } from "app/services/AuthService";
import { Http } from "@angular/http/";
import { ShippingModel } from "app/models/ShippingModel";
import { HttpParams } from "@angular/common/http/";
import { ShoppingItemModel } from "app/models/Shopping-item";
import { OrderRow } from "app/models/OrderRowModel";
import { MatSnackBar } from "@angular/material";

@Injectable()
export class WinkelwagenService{
    url: string;
    constructor(private http: HttpClient, private httpN: Http, private auth: AuthService, private snackBar: MatSnackBar){
        this.url = auth.apiUrl();
    }

    getCityName(postcode: number){
        let url = `http://api.zippopotam.us/NL/`+postcode;
        return this.http.get(url);
    }
    
    public sendOrderToDatabase(order:OrderModel):void{
        const headers = this.auth.createAuthHttpHeader();
        let userFk = 0;
        if(this.auth.isUser()){
            userFk = this.auth.loggedUserObject.userId;
        }
        let data =
        {
            orderRowList: order.orderRowList,
            orderTotalPrice: order.orderTotalPrice,
            shipping: order.shipping,
            userId: userFk
        };
        this.http.post(this.url+`orders/create`, data, {headers:headers}).subscribe
        (
            data =>
            {
                if(data != true){
                    this.snackBar.open('Er is iets fout gegaan met de server','',{duration:1000});
                }else{
                    sessionStorage.removeItem('winkelwagen');
                    location.reload();
                    this.snackBar.open('Succesvol','',{duration:1000});
                }
            },
            error =>{
                this.snackBar.open('Er is iets fout gegaan','',{duration:1000});
            }
        );
        
    }
}