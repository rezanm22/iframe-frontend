import { Component, OnInit } from '@angular/core';
import { ProductModel } from 'app/models/ProductModel';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ProductService } from 'app/services/ProductService';
import { CategorieService } from 'app/services/CategorieService';
import { CategorieModel } from 'app/models/CategorieModel';
import { ActivatedRoute } from '@angular/router';
import { ShoppingItemModel } from 'app/models/Shopping-item';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-producten',
  templateUrl: './producten.component.html',
  styleUrls: ['./producten.component.css']
})
export class ProductenComponent {
  events = [];
  cat: number;
  products: ProductModel[]=[];
  categories: CategorieModel[];
  filteredProductes: ProductModel[]=[];
  constructor(private snackBar:MatSnackBar, private route: ActivatedRoute, private productService: ProductService, private catService: CategorieService) { 
    productService.getAllProducts().subscribe(products => {
      this.products = products;

      route.queryParamMap.subscribe(params =>{
        this.cat = parseInt(params.get('category'));
 
        this.filteredProductes = (this.cat) ?
        this.products.filter(p => 
         p.catId === this.cat):
         this.products;
     });
    });
    catService.getAll().subscribe(categories =>{
      this.categories = categories;
    });

  }

  addToChart(product: ProductModel)
  { 
    
    let storage = false ? localStorage : sessionStorage;
    let check:  ShoppingItemModel[]= [];
    if(storage.getItem('winkelwagen') != null){
      check = JSON.parse(storage.getItem('winkelwagen'));
      for(let item of check){
        if(item.product.productId == product.productId){
          check = JSON.parse(storage.getItem('winkelwagen'));
          this.snackBar.open(
            "Artikel is al toegevoegd in de winkelwagen. Klik <a routerLink='winkelwagen'> Hier om naar winkelwagen te gaan. </a> ",  '', {
            duration: 1000
          });
          check.splice(check.indexOf(item),1);
          break;
        }
      }
    }
    check.push(new ShoppingItemModel(product, 1));
    storage.setItem('winkelwagen', JSON.stringify(check));
    this.snackBar.open(
      "Artikel toegevoegd in de winkelwagen.", '', {
      duration: 1000
    });
  }

}