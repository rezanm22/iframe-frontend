import { Component, OnInit } from '@angular/core';
import { ShoppingItemModel } from 'app/models/Shopping-item';
import { WinkelwagenService } from 'app/services/CartService';
import { OrderModel } from 'app/models/OrderModel';
import { ShippingModel } from 'app/models/ShippingModel';
import { MatSnackBar } from '@angular/material';
import { OrderRow } from 'app/models/OrderRowModel';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent {
  totalPrice: number = 0;
  regExPostcode = "([0-9]{4}[A-Z]{2})";
  cart: ShoppingItemModel[] = [];
  shipping: ShippingModel = new ShippingModel();
  order: OrderModel;
  placeName: string;
  storage = false ? localStorage:sessionStorage;
  constructor(private cartService: WinkelwagenService, private snackBar: MatSnackBar) {
    
    if(JSON.parse(sessionStorage.getItem("winkelwagen")))
    {
      this.cart = JSON.parse(sessionStorage.getItem("winkelwagen"));
      for(let item of this.cart){
        this.totalPrice += item.product.productPrice * item.quantity;
      }
    }
    
   }

   searchCityName(postcode: string){
     if(postcode.length == 6){
     let pCode: number = parseInt(postcode.substring(0,4));
    this.cartService.getCityName(pCode).subscribe(data =>{
      for(let i of data['places']){
        this.placeName = i["place name"];
      }
    });
   }
  }

  placeOrder(){
    this.shipping.city = this.placeName;
    let productNrs: OrderRow[] = [];
    for(let id of this.cart){
      productNrs.push(new OrderRow(id.product.productId, id.quantity));
    }
    this.order = new OrderModel(productNrs,this.totalPrice, this.shipping);
    this.snackBar.open("Bestelling is geplaatst. ",'',{duration: 1000});
    this.cartService.sendOrderToDatabase(this.order);
  }
}
