import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from 'app/models/UserModel';
import { AuthService } from 'app/services/AuthService';
import { UserService } from 'app/services/UserService';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent {
  currentUser: UserModel;
  isUser: boolean = false;
  constructor(private router: Router, private auth: AuthService, private userService: UserService) { 
    if(auth.isAuthorized()){
      this.currentUser = auth.loggedUserObject;
      if(this.currentUser.roleName == 'user'){
        this.isUser = true;
      }
    }
  }
  goToPortal(){
    this.router.navigate(['admin/login']);
  }
  logout(){
    this.userService.logOut();
    this.router.navigate(['/products']);
  }
}
