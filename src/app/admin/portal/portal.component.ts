import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/UserService';
import { Router } from '@angular/router';
import { AuthService } from 'app/services/AuthService';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.css']
})
export class PortalComponent implements OnInit{

  isAdmin: boolean = false;
  rememberMe = false;
  constructor(private userService: UserService, private auth: AuthService, private route: Router, private snaBar: MatSnackBar) {
    if(auth.isAdmin()){
      this.isAdmin = true;
    }
  }

  ngOnInit() {
    if(!this.auth.isAdmin())
    {
      this.route.navigate(['/admin/login']);
      this.snaBar.open('Log eerst als admin in. ');
    }
  }

}
