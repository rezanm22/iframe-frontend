import { Component, OnInit } from '@angular/core';
import { UserModel } from 'app/models/UserModel';
import { UserService } from 'app/services/UserService';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class AdminLoginComponent{

  currentAdminUser: UserModel;
  rememberMe = false;
  wait = false;
  constructor(private userService: UserService, private auth: AuthService, private route: Router, snaBar: MatSnackBar) {
    if(auth.isAdmin()){
      route.navigate(['admin/portal']);
    }else if(auth.isAuthorized() && !auth.isAdmin()){
      snaBar.open("Authorizatie mislukt",'',{duration:1000});
    }else{}
  }
  rememberMeChecked(){
    if(this.rememberMe){this.rememberMe = false}
    else{this.rememberMe = true}
    
  }
  login(email: string, password: string){
    this.wait = true;
     this.userService.login(this.rememberMe, email, password);
  }

}
