import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductenBeherenComponent } from './producten-beheren.component';

describe('ProductenBeherenComponent', () => {
  let component: ProductenBeherenComponent;
  let fixture: ComponentFixture<ProductenBeherenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductenBeherenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductenBeherenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
