import { Component, OnInit } from '@angular/core';
import { ProductModel } from 'app/models/ProductModel';
import { ProductService } from 'app/services/ProductService';
import { CategorieModel } from 'app/models/CategorieModel';
import { CategorieService } from 'app/services/CategorieService';
import { FormControl } from '@angular/forms';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router/';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-modify-product',
  templateUrl: './modify-product.component.html',
  styleUrls: ['./modify-product.component.css']
})
export class ModifyProductComponent implements OnInit {
  private selectedProduct: ProductModel = new ProductModel();
  private categories: CategorieModel[] = [];
  panelCatId = new FormControl();
  isAdmin: boolean = false;

  constructor(private productService: ProductService, private catService: CategorieService, private auth: AuthService, private router: Router, private snaBar: MatSnackBar) { }

  ngOnInit() {
    if(!this.auth.isAdmin())
    {
      this.router.navigate(['/admin/login']);
      this.snaBar.open('Log eerst als admin in. ');
    }
    this.productService.product$.forEach(product=>{
      this.selectedProduct = product;
      this.panelCatId = new FormControl(product.catId);
    });
    this.catService.getAll().subscribe(res=>{
      this.categories = res;
    });
    this.isAdmin = this.auth.isAdmin();
    
  }
  toggle(){
    this.productService.triggerEvent(true);
  }
  modifyProduct(){
    this.productService.updateProduct(this.selectedProduct);
    this.productService.triggerEvent(true);
  }

}
