import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatTableDataSource, MatFormFieldModule, MatInputModule, MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatSidenav, MatSnackBar } from '@angular/material';
import { ProductModel } from 'app/models/ProductModel';
import { ProductService } from 'app/services/ProductService';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router/';
import { CategorieModel } from 'app/models/CategorieModel';
import { CategorieService } from 'app/services/CategorieService';
import { DialogsService } from 'app/services/DialogService';
import { FormControl } from '@angular/forms/';

@Component({
  selector: 'app-producten-beheren',
  templateUrl: './producten-beheren.component.html',
  styleUrls: ['./producten-beheren.component.css']
})
export class ProductenBeherenComponent implements OnInit{

  panelCatId = new FormControl();
  private dataSource: MatTableDataSource<ProductModel>;
  private editClicked: boolean = false;
  displayedColumns = ['productName', 'productPrice', 'productId','productDelete'];
  selectedProduct: ProductModel = new ProductModel();
  selectedCategorie: number;
  categories: CategorieModel[];
  isAdmin: boolean = false;
  constructor(
    private productService: ProductService,private auth: AuthService, 
    private router: Router, catService: CategorieService, private route: Router, private snaBar: MatSnackBar,
    public dialogService: DialogsService) {
    this.isAdmin = auth.isAdmin();
    catService.getAll().subscribe(categories =>{
      this.categories = categories;
    });
  }
  ngOnInit(){
      if(!this.auth.isAdmin())
      {
        this.route.navigate(['/admin/login']);
        this.snaBar.open('Log eerst als admin in. ');
      }
    this.productService.getAllProductsHttpClient().subscribe((data) => {
      this.dataSource = new MatTableDataSource<ProductModel>(data);
    })
  }

  reloadProducts(){
    this.ngOnInit();
  }

  //  MatTableDataSource function
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  selectRow(row: ProductModel) {
    this.selectedProduct = row;
    this.selectedCategorie = row.catId;
    this.panelCatId = new FormControl(row.catId);
  }
  modifyProduct()
  {
    this.selectedProduct.catId = this.selectedCategorie;
    this.productService.updateProduct(this.selectedProduct);
  }
  deleteProduct()
  {
    setTimeout(()=>{
      this.productService.removeProduct(this.selectedProduct.productId);
    },500);
    
  }

  backToLogin(){
    this.router.navigate(['/admin/portal']);
  }
  editTrue(){
    //this.productService.setProductToModify(this.selectedProduct);
    this.editClicked = true;
  }
  editFalse(){
    this.editClicked = false;
  }
  openDialog(){
    this.dialogService.confirm('Bevestiging dialog','Weet u zeker dat dit product wilt verwijderen? ').subscribe(res=>{
      if(res != true){
        console.log("NOPE");
      }else{
        this.deleteProduct();
      }
    })
  }
  openCreateDialog(){
    this.dialogService.createProduct();
  }

}
