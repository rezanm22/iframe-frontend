import { Component, OnInit } from '@angular/core';
import { ProductService } from 'app/services/ProductService';
import { ProductModel } from 'app/models/ProductModel';
import { CategorieService } from 'app/services/CategorieService';
import { CategorieModel } from 'app/models/CategorieModel';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router/';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit{
  product: ProductModel = new ProductModel();
  selectedCategorie: number;
  categories: CategorieModel[];
  isAdmin: boolean;
  regExPrice = "^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|0)?\.[0-9]{1,2}$";
  constructor(private productService: ProductService, private catService: CategorieService, private auth: AuthService, private router: Router
    , private dialogRef: MatDialogRef<any>, private snaBar: MatSnackBar) { 
    this.isAdmin = auth.isAdmin();
    catService.getAll().subscribe(cats =>{
      this.categories = cats;
    })
  }

  createProduct() {
    this.product.catId = this.selectedCategorie;
    this.productService.insertNewProduct(this.product);
    this.dialogRef.close();
  }
  ngOnInit(){
    if(!this.auth.isAdmin())
    {
      this.router.navigate(['/admin/login']);
      this.snaBar.open('Log eerst als admin in. ');
    }
  }

}
