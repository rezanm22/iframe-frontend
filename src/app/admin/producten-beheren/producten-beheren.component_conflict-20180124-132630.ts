import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatTableDataSource, MatFormFieldModule, MatInputModule, MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatSidenav, MatSnackBar } from '@angular/material';
import { ProductModel } from 'app/models/ProductModel';
import { ProductService } from 'app/services/ProductService';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router/';
import { CategorieModel } from 'app/models/CategorieModel';
import { CategorieService } from 'app/services/CategorieService';
import { DialogsService } from 'app/services/DialogService';
import { FormControl } from '@angular/forms/';

@Component({
  selector: 'app-producten-beheren',
  templateUrl: './producten-beheren.component.html',
  styleUrls: ['./producten-beheren.component.css']
})
export class ProductenBeherenComponent implements OnInit{
  @ViewChild('sidenav') sidenav: MatSidenav;
  panelCatId = new FormControl();
  private dataSource: MatTableDataSource<ProductModel>;
  private editClicked: boolean = false;
  displayedColumns = ['productName', 'productPrice', 'productId','productDelete'];
  selectedProduct: ProductModel = new ProductModel();
  selectedCategorie: number;
  categories: CategorieModel[];
  isAdmin: boolean = false;
  constructor(
    private productService: ProductService,private auth: AuthService, 
    private router: Router, catService: CategorieService, private route: Router, private snaBar: MatSnackBar,
    public dialogService: DialogsService) {
    this.isAdmin = auth.isAdmin();
    catService.getAll().subscribe(categories =>{
      this.categories = categories;
    });
    this.loadData();
  }
  ngOnInit(){
      if(!this.auth.isAdmin())
      {
        this.route.navigate(['/admin/login']);
        this.snaBar.open('Log eerst als admin in. ');
      }
      this.productService.event$.forEach(res=>{
        console.log(res)
        if(res == true){
          setTimeout(()=>{ this.loadData();},1000);
          if(this.sidenav.opened){
            this.sidenav.toggle();
          }
        }
      });
  }
  loadData(){
    this.productService.getAllProductsHttpClient().subscribe((data) => {
      this.dataSource = new MatTableDataSource<ProductModel>(data);
    });
  }
  //  MatTableDataSource function
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  selectRow(row: ProductModel) {
    this.selectedProduct = row;
    this.productService.sendToModifyProduct(row);
  }
  deleteProduct()
  {
    this.productService.removeProduct(this.selectedProduct.productId);
  }

  backToLogin(){
    this.router.navigate(['/admin/portal']);
  }
  openDialog(){
    this.dialogService.confirm('Bevestiging dialog','Weet u zeker dat dit product wilt verwijderen? ').subscribe(res=>{
      if(res != true){
      }else{
        this.deleteProduct();
        setTimeout(()=>{this.loadData()},1000);
      }
    })
  }
  openCreateDialog(){
    this.dialogService.createProduct();
  }

}
