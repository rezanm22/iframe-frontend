import { Component, OnInit, ViewChild } from '@angular/core';
import { UserModel } from 'app/models/UserModel';
import { MatTableDataSource, MatDialog, MatSnackBar, MatSidenav } from '@angular/material';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router';
import { UserService } from 'app/services/UserService';
import { Roles } from 'app/models/Roles';
import { DialogContentComponent } from 'app/home/dialog-content/dialog-content.component';
import { DialogsService } from 'app/services/DialogService';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-users-management',
  templateUrl: './users-management.component.html',
  styleUrls: ['./users-management.component.css']
})
export class UsersManagementComponent implements OnInit{
  @ViewChild('sidenav') sidenav: MatSidenav;
  panelRoleId = new FormControl();
  private dataSource: MatTableDataSource<UserModel>;
  private editClicked: boolean = false;
  displayedColumns = ['userName', 'userEmail','userRole', 'userModify','userDelete'];
  selectedUser: UserModel = new UserModel();
  isAdmin: boolean = false;
  public result: any;
  selectedRole: number;
  constructor(private userService: UserService,private auth: AuthService, 
    private router: Router, private dialogsService: DialogsService, private snackBar: MatSnackBar) {
    this.isAdmin = auth.isAdmin();
    this.loadData();
  }
  ngOnInit() {
    if(!this.auth.isAdmin())
      {
        this.router.navigate(['/admin/login']);
        this.snackBar.open('Log eerst als admin in. ');
      }
    this.userService.event$.forEach(res=>{
      console.log(res)
      if(res == true){
        setTimeout(()=>{ this.loadData();},1000);
        if(this.sidenav.opened){
          this.sidenav.toggle();
        }
      }
    });
  }
  loadData(){
    this.userService.getAllUsers().subscribe((data) => {
      this.dataSource = new MatTableDataSource<UserModel>(data)
    });
  }
  //  MatTableDataSource function
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  selectRow(row: UserModel) {
    this.selectedUser = row;
    this.userService.sendToModifyUser(row);
  }
  deleteUser()
  { 
    this.userService.removeUser(this.selectedUser.userId);
  }

  backToLogin(){
    this.router.navigate(['/admin/portal']);
  }
  public openDialog() {
    this.dialogsService
      .confirm('Bevestiging Dialog', 'Weet u zeker dat u deze gebruiker wilt verwijderen?')
      .subscribe(res => {
        if(res != true){
          this.result = " NOPE ";
        }else{
          this.deleteUser();
          setTimeout(()=>{this.loadData()},500);
        }
      });
      
  }
  openCreateDialog(){
    this.dialogsService.createUser();
  }

}
