import { Component, OnInit } from '@angular/core';
import { UserModel } from 'app/models/UserModel';
import { UserService } from 'app/services/UserService';
import { AuthService } from 'app/services/AuthService';
import { Roles } from 'app/models/Roles';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router/';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent  {

  user: UserModel = new UserModel();
  selectedRole: string;
  roles: Roles[] = [];
  isAdmin: boolean;
  constructor(private userService: UserService, private auth: AuthService, private dialogRef: MatDialogRef<any>, private snaBar: MatSnackBar
  ,private router: Router) { 
    this.isAdmin = auth.isAdmin();
    userService.getRoles().subscribe(roles =>{
      this.roles = roles;
    })
  }

  createUser() {
    this.userService.createUser(this.user);
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if(!this.auth.isAdmin())
    {
      this.router.navigate(['/admin/login']);
      this.snaBar.open('Log eerst als admin in. ');
    }
  }

}
