import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/UserService';
import { UserModel } from 'app/models/UserModel';
import { FormControl } from '@angular/forms';
import { Roles } from 'app/models/Roles';
import { AuthService } from 'app/services/AuthService';

@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styleUrls: ['./modify-user.component.css']
})
export class ModifyUserComponent implements OnInit {

  selectedUser: UserModel = new UserModel();
  panelRoleId = new FormControl();
  roles: Roles[] = [];
  isAdmin: boolean = false;
  constructor(private userService: UserService, auth: AuthService) { 
    userService.getRoles().subscribe(res=>{
      this.roles = res;
    })
    this.isAdmin = auth.isAdmin();
    
  }
  toggle(){
    this.userService.triggerEvent(true);
  }
  ngOnInit() {
    this.userService.user$.forEach(res=>{
      this.selectedUser = res;
      this.panelRoleId = new FormControl(res.roleId)
    })
  }
  modifyUser(){
    this.userService.updateUserInfo(this.selectedUser);
    this.userService.triggerEvent(true);
  }

}
