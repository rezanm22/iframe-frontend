import { Component, OnInit } from '@angular/core';
import { DialogsService } from 'app/services/DialogService';
import { CategorieService } from 'app/services/CategorieService';
import { CategorieModel } from 'app/models/CategorieModel';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { AuthService } from 'app/services/AuthService';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories-management',
  templateUrl: './categories-management.component.html',
  styleUrls: ['./categories-management.component.css']
})
export class CategoriesManagementComponent implements OnInit{
  panelOpenState: boolean = false;
  categories: CategorieModel[]=[];
  private dataSource: MatTableDataSource<CategorieModel>;
  displayedColumns = ['categorieId', 'categorieName','categorieDelete'];
  selectedCategorie: CategorieModel = new CategorieModel();
  isAdmin: boolean = false;
  constructor(private dialogService: DialogsService, 
    private route: Router,private categorieService: CategorieService, private auth: AuthService, private snaBar:MatSnackBar) { 
    this.isAdmin = auth.isAdmin();
  }
  ngOnInit(){
    if(!this.auth.isAdmin())
    {
      this.route.navigate(['/admin/login']);
      this.snaBar.open('Log eerst als admin in. ');
    }
    this.categorieService.getAll().subscribe(categories=>{
      this.categories = categories;
      this.dataSource = new MatTableDataSource<CategorieModel>(categories);
    });
  }
  //  MatTableDataSource function
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  selectRow(row) {
    this.selectedCategorie = row
  }
  openDialog(){
    this.dialogService.confirm('Bevestiging dialog','Weet u zeker dat u deze categorie wilt verwijderen? ', 'LET OP: alle producten van deze categorie worden ook verwijderd.').subscribe(res=>{
      if(res != true){
        console.log("NOPE");
      }else{
        this.categorieService.removeCategorie(this.selectedCategorie.catId);
      }
     
    })
  }
  reloadCategories(){
    this.ngOnInit();
  }
  openCreateCategorie(){
    this.dialogService.createCategorie();
  }
}
