import { Component, OnInit } from '@angular/core';
import { CategorieModel } from 'app/models/CategorieModel';
import { ProductService } from 'app/services/ProductService';
import { CategorieService } from 'app/services/CategorieService';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create-categorie',
  templateUrl: './create-categorie.component.html',
  styleUrls: ['./create-categorie.component.css']
})
export class CreateCategorieComponent implements OnInit{


  categorie: CategorieModel = new CategorieModel();
  selectedCategorie: number;
  isAdmin: boolean;
  constructor(private catService: CategorieService, private auth: AuthService, private router: Router, private dialogRef: MatDialogRef<any>
  ,private route: Router, private snaBar: MatSnackBar) { 
    this.isAdmin = auth.isAdmin();
  }

  createCategorie() {
    this.catService.createCategorie(this.categorie);
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if(!this.auth.isAdmin())
    {
      this.route.navigate(['/admin/login']);
      this.snaBar.open('Log eerst als admin in. ');
    }
  }
}
