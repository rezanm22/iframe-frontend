import { Component, OnInit } from '@angular/core';
import { UserModel } from 'app/models/UserModel';
import { UserService } from 'app/services/UserService';
import { AuthService } from 'app/services/AuthService';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {


  notLoggedIn: boolean = false;
  rememberMe = false;
  wait = false;
  constructor(private userService: UserService, private auth: AuthService, private route: Router, snackBar: MatSnackBar) {
    if(auth.isAuthorized()){
      if(auth.loggedUserObject.roleName == 'user'){
        this.route.navigate(['']);
        snackBar.open('U bent al ingelogd.','',{duration: 1000});
      }
    }
  }
  rememberMeChecked(){
    if(this.rememberMe){this.rememberMe = false}
    else{this.rememberMe = true}
    
  }
  login(email: string, password: string){
    this.wait = true;
     this.userService.login(this.rememberMe, email, password);
     
  }

}
