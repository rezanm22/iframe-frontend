import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/services/AuthService';
import { UserModel } from 'app/models/UserModel';
import { UserService } from 'app/services/UserService';
import { Router } from '@angular/router/';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent  {
user: UserModel = new UserModel();
isUser:boolean = false;
change: boolean = true;
  constructor(private auth: AuthService, private userService: UserService, private router: Router) {
    if(this.auth.isUser()){
      this.user = auth.loggedUserObject;
      this.isUser = true;
    }
   }

   update(){
    this.change = true;
  }
  changeUser(){
    console.log(this.user);
    this.userService.updateUserInfo(this.user);
    this.userService.logOut();
    this.router.navigate(['/user/login']);
  }

}
