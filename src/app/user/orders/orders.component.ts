import { Component, OnInit } from '@angular/core';
import { OrderModel } from 'app/models/OrderModel';
import { UserService } from 'app/services/UserService';
import { AuthService } from 'app/services/AuthService';
import { MatTableDataSource } from '@angular/material';
import { OrderRow } from 'app/models/OrderRowModel';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  private dataSource: MatTableDataSource<OrderModel>;
  displayedColumns = ['orderId', 'orderDate', 'OrderTotalprice', 'orderShippingAddress'];



  selectedOrder: OrderModel;
  alOrders: OrderModel[];
  private dataSourceRow: MatTableDataSource<OrderRow>;
  displayedColumnsRow = ['productId', 'productName', 'productQuantity'];
  constructor(private userService: UserService, private auth: AuthService) {
    userService.getUserOrders(auth.loggedUserObject).subscribe(orders =>{
      this.alOrders = orders;
    })
    this.loadData().then((data) => {
      this.dataSource = new MatTableDataSource<OrderModel>(data);
    }, (error) => console.log(error.SessionNotCreatedError));
  }

  //  MatTableDataSource function
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  //  Return promise to use to fill data
  //  !! IMPORTANT THING TO NOTE IS WE HAVE TO WAIT UNTIL WE COMPLETE THE DATA REQUEST BEFORE SHOWING !!
  loadData(): Promise<OrderModel[]> {
    return this.userService.getUserOrders(this.auth.loggedUserObject)
      .toPromise()
      .then(res => res)
      .then(products => products.map(order => {
        return new OrderModel(
          order.orderRowList,
          order.orderTotalPrice,
          order.shipping,
          order.userId,
          order.orderId,
          order.orderDate);
      }));
  }
   selectRow(row) {
    this.selectedOrder = row
    this.dataSourceRow = new MatTableDataSource<OrderRow>(this.selectedOrder.orderRowList);
  }
  ngOnInit() {
  }

}
