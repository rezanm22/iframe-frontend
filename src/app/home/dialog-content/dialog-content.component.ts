import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { UsersManagementComponent } from 'app/admin/users-management/users-management.component';

@Component({
  selector: 'app-dialog-content',
  templateUrl: './dialog-content.component.html',
  styleUrls: ['./dialog-content.component.css']
})
export class DialogContentComponent{

    public title: string;
    public message: string;
    public warning: string;

    constructor(public dialogRef: MatDialogRef<any>) {

    }

}
